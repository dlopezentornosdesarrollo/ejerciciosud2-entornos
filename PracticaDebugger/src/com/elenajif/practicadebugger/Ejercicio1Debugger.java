package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio1Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las
		 * variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();

		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
			/*
			 * El error se produce porque tenemos un bucle for en el que indicamos que el valor de la variable i
			 * puede aumentar hasta el tama�o de la cadena o un n�mero menor.
			 * Cuando llega al tama�o exacto de la cadena, en este caso 4, como comienza la variable i con valor 0
			 * y suma de 1 en 1, lo que ocurre es que en la l�nea: "caracter = cadenaLeida.charAt(i);" 
			 * la variable "caracter" toma el valor de un car�cter introduci�ndole la posici�n con la variable i y el m�todo
			 * .charAt(). Entonces empieza con 0, es decir, charAt(0); y as� sucesivamente hasta el valor 4  y como la posici�n
			 * 4 en este caso no existe te genera el error.
			 */
			/*
			 * POSIBLE SOLUCI�N:
			 * Lo que habr�a que hacer es modificar la l�nea de c�digo:
			 * 
			 * for (int i = 0; i <= cadenaLeida.length(); i++) {
			 * 
			 * 
			 * Por esta otra:
			 * 
			 * for (int i = 0; i < cadenaLeida.length(); i++) {
			 * 
			 * 
			 * De esta forma ya no tomar� la variable i el valor del tama�o de la cadena y no saltar� el error, dado
			 * que s�lo llegar� hasta la posici�n 3 en este caso y no saltar� el error.
			 */
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
