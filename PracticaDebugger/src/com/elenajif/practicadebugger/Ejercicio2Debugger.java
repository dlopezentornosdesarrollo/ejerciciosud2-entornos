package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			/*
			 * El error se produce porque la variable i toma el valor 0 y en la l�nea de c�digo: 
			 * resultadoDivision = numeroLeido / i;
			 * al dividir por 0 salta el error, porque dividir por 0 no da un valor natural digamos en matem�ticas.
			 */
			
			/*
			 * POSIBLE SOLUCI�N:
			 * Modificar�a la l�nea de c�digo:
			 * 
			 * 
			 * for(int i = numeroLeido; i >= 0 ; i--){
			 * 
			 * 
			 * por esta otra:
			 * 
			 * 
			 * for(int i = numeroLeido; i > 0 ; i--){
			 * 
			 * De esta forma, evitaremos que la variable i tome el valor 0 y no nos saltar� el error.
			 */
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
				
		lector.close();
	}

}
